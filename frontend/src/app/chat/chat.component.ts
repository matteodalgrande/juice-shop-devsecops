import { Component, OnInit } from '@angular/core';
import { ChatService } from '../Services/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
  providers: [ChatService]
})

export class ChatComponent implements OnInit {

  user: String;
  room: String;
  messageText: String;
  messageArray: Array<{ user: String, message: String }> = [];

  constructor(private _chatService: ChatService) { }
  ngOnInit() {
    this._chatService.newUserJoined()
      .subscribe(data => this.messageArray.push(data));

    this._chatService.userLeftRoom()
      .subscribe(data => this.messageArray.push(data));

    this._chatService.newMessageReceived()
      .subscribe(data => this.messageArray.push(data));

    this._chatService.privacyRecived()
      .subscribe((data: { user: String; }) => {
        for(var i = this.messageArray.length - 1; i >= 0; i--) {
          if(this.messageArray[i].user === data.user  && i != -1) {
            this.messageArray.splice(i, 1);
          }
        }
      });
  }

  join() {
    this._chatService.joinRoom({ user: this.user, room: this.room });
  }
  leave() {
    this._chatService.leaveRoom({ user: this.user, room: this.room });
  }
  sendMessage() {
    this._chatService.sendMessage({ user: this.user, room: this.room, message: this.messageText });
  }
  // privacy() {
  //   this._chatService.privacy({ user: this.user });
  //   console.log('privacy event in chat.component');
  // }
}
