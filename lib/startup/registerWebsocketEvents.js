/*
 * Copyright (c) 2014-2020 Bjoern Kimminich.
 * SPDX-License-Identifier: MIT
 */

const notifications = require('../../data/datacache').notifications
const utils = require('../utils')
const insecurity = require('../insecurity')
const challenges = require('../../data/datacache').challenges
const config = require('config')
let firstConnectedSocket = null

const registerWebsocketEvents = (server) => {
  const io = require('socket.io')(server)
  global.io = io

  io.on('connection', socket => {
    if (firstConnectedSocket === null) {
      socket.emit('server started')
      firstConnectedSocket = socket.id
    }

    notifications.forEach(notification => {
      socket.emit('challenge solved', notification)
    })

    socket.on('notification received', data => {
      const i = notifications.findIndex(({ flag }) => flag === data)
      if (i > -1) {
        notifications.splice(i, 1)
      }
    })

    socket.on('verifyLocalXssChallenge', data => {
      utils.solveIf(challenges.localXssChallenge, () => { return utils.contains(data, '<iframe src="javascript:alert(`xss`)">') })
      utils.solveIf(challenges.xssBonusChallenge, () => { return utils.contains(data, config.get('challenges.xssBonusPayload')) })
    })

    socket.on('verifySvgInjectionChallenge', data => {
      utils.solveIf(challenges.svgInjectionChallenge, () => { return data && data.match(/.*\.\.\/\.\.\/\.\.\/\.\.[\w/-]*?\/redirect\?to=https?:\/\/placekitten.com\/(g\/)?[\d]+\/[\d]+.*/) && insecurity.isRedirectAllowed(data) })
    })


    socket.on('join', data => {
      //joining
      console.log('data-->'+data);
      console.log('room-->'+data.room);
      socket.join(data.room);
      console.log(data.user + 'joined the room : ' + data.room);
      socket.broadcast.to(data.room).emit('new user joined', {user:data.user, message:'has joined this room.'});
    });
    socket.on('leave', data => {
      console.log(data.user + 'left the room : ' + data.room);
      socket.broadcast.to(data.room).emit('left room', {user:data.user, message:'has left this room.'});
      socket.leave(data.room);
    });
    socket.on('message',data => {
      console.log('message-->'+data.user+' '+data.message)
      io.in(data.room).emit('new message', {user:data.user, message:data.message});
    });
    socket.on('privacy', data => {
      console.log('privacy event');
      io.in(data.room).emit('privacy', {user:data.user});
    });


  })
}

module.exports = registerWebsocketEvents
